import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login/main'
import SignUp from '@/components/SignUp/main'
import Dashboard from '@/components/Dashboard/main'
import Orders from '@/components/Orders/main'
import Catalogue from '@/components/Catalogue/main'
import ShowList from '@/components/Catalogue/showlist'
import AddCatalogue from '@/components/Catalogue/add'
import Inventory from '@/components/Inventory/main'
import Customers from '@/components/Customers/main'
import Reports from '@/components/Reports/main'
import Settings from '@/components/Settings/main'
import Support from '@/components/Support/main'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/orders',
      name: 'Orders',
      component: Orders
    },
    {
      path: '/catalogue',
      name: 'Catalogue',
      component: Catalogue,
      children: [
        {
          path: '/',
          name: 'ShowList',
          component: ShowList},
        { path: '/catalogue/add-listing',
          name: 'AddCatalogue',
          component: AddCatalogue}
      ]
    },
    {
      path: '/inventory',
      name: 'Inventory',
      component: Inventory
    },
    {
      path: '/customers',
      name: 'Customers',
      component: Customers
    },
    {
      path: '/reports',
      name: 'Reports',
      component: Reports
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/support',
      name: 'Support',
      component: Support
    }
  ]
})
