import firebase from 'firebase'

let config = {

  apiKey: 'AIzaSyAd1hItMUqq0DgHG3KSYetxnuHTAYMeEDA',
  authDomain: 'cropchat-10c6c.firebaseapp.com',
  databaseURL: 'https://cropchat-10c6c.firebaseio.com',
  projectId: 'cropchat-10c6c',
  storageBucket: 'cropchat-10c6c.appspot.com',
  messagingSenderId: '10878953956'
}

firebase.initializeApp(config)

export default {
  database: firebase.database()
}
